# Errors handling

Программ без ошибок не бывает. Их количество можно уменьшить с помощью системы типов, линтеров, тестов или даже целого отдела тестировщиков, но убрать совсем невозможно. Это реальность с которой мы живем и лучшее, что можно сделать, научиться эти ошибки отслеживать и быстро исправлять. Чем сложнее система, тем меньше шансов на продакшене выше воспроизвести ошибку. Каким образом отлаживать код в такой ситуации? Здесь мы приходим к главной идее отладки. Правильная отладка — это хорошо настроенное логирование.

В этом задании вам необходимо будет зарегистрироваться в [Rollbar](https://rollbar.com/) и подключить его к [hexletcomponents/devops-example-app](https://hub.docker.com/r/hexletcomponents/devops-example-app). При при запросе страницы по адресу */error* возвращается ошибка и сообщение о ней. Если была указана верно переменная окружения `ROLLBAR_TOKEN`, то информация об ошибке будет отправляться в Rollbar.

Ниже продемонстрирован пример запроса с помощью [httpie](https://httpie.io/):

```sh
$ http http://example.com/error
HTTP/1.1 500 Internal Server Error
Connection: keep-alive
Date: Tue, 18 May 2021 14:43:07 GMT
Keep-Alive: timeout=5
content-length: 299
content-type: text/html; charset=utf-8

<!DOCTYPE html><html><head><title>DevOps Example App</title><link href="/assets/css/bootstrap.min.css" rel="stylesheet"></head><body></body><div class="alert alert-danger"><h1 class="alert-heading">Внимание, тут что-то не так!</h1><p>Oops! Something went wrong!</p></div></html>
```

## Ссылки

* [Rollbar](https://rollbar.com/)
* [DevOps Example App](https://github.com/hexlet-components/devops-example-app)
* [Что такое логирование?](https://guides.hexlet.io/logging/)
* [Что такое трекинг ошибок?](https://guides.hexlet.io/error-tracking/)
* [hexlet-basics](https://github.com/hexlet-basics/hexlet-basics/tree/master/ansible)

## Задачи

* Зарегистрируйтесь на Rollbar и создайте проект.
* Задеплойте с помощью Ansible приложение. Задайте зашифрованную переменную окружения `ROLLBAR_TOKEN` с токеном Rollbar.
* Откройте страницу */error* и убедитесь, что в Rollbar информация об ошибке была передана.
* Когда будете отправлять домашнее задание на проверку, приложите скриншот ошибки в Rollbar и адрес по которому открывается страница с ошибкой, например *http:example.club:5000/error* .

![rollbar example log item](assets/rollbar-example.png)
